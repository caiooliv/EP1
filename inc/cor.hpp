#ifndef COR_HPP
#define COR_HPP

#include <iostream>

using namespace std;


class Cor{

private:
	unsigned char R;
	unsigned char G;
	unsigned char B;

public:
	Cor();
	Cor(unsigned char R,unsigned char G,unsigned char B);

	unsigned char getR();

	void setR(unsigned char R);

	unsigned char getG();

	void setG(unsigned char G);

	unsigned char getB();

	void setB(unsigned char B);


};

#endif