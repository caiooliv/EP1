#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class Negativo : public Filtro {

	public:
		
	Negativo();

	list<Cor> aplicaFiltro(list<Cor> cores, int escalaMaxima);

};

#endif