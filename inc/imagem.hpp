#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <list>
#include "cor.hpp"


using namespace std;

class Imagem {

private:
	
	string formato;
	int largura;
	int altura;
	int escalaMaxima;
	list <Cor>  cores;
	int cabecalho;
	string diretorio;

public:
		
	Imagem();
	Imagem(string formato, int altura, int largura, int escalaMaxima, list<Cor> cores);


	string getFormato();
	void setFormato(string formato);

	int getLargura();
	void setLargura(int largura);

	int getAltura();
	void setAltura(int altura);

	int getEscalaMaxima();
	void setEscalaMaxima(int escalaMaxima);

	string getDiretorio();
	void setDiretorio(string diretorio);

	void lerImagem (ifstream * arquivo);

	void novaImagem ();

	void ignorarComentario (ifstream *arquivo);

	int getCabecalho();
	void setCabecalho (int cabecalho);

	list <Cor> getCores();
	void setCores(list<Cor> cores);

};

#endif		