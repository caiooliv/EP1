#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class Polarizado : public Filtro{
	public:
		Polarizado();
		list<Cor> aplicaFiltro(list<Cor> cores, int escalaMaxima);
};
#endif