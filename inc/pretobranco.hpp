#ifndef PRETOBRANCO_HPP
#define PRETOBRANCO_HPP

#include "filtro.hpp"
#include "imagem.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class PretoBranco : public Filtro{
	public:
		PretoBranco();
		list<Cor> aplicaFiltro(list<Cor> cores, int escalaMaxima);
};
#endif