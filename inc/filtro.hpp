#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "cor.hpp"
#include "imagem.hpp"

#include <list>
#include <iostream>

using namespace std;

class Filtro
{	

protected:
	list<Cor> cor;
	list<Cor>::iterator aux;


public:
	Filtro();
	list<Cor> aplicaFiltro(list<Cor> cores,int escalaMaxima);

	void setCores(list<Cor> Cores);
	list<Cor> getCores(); 	

	
	
};





#endif