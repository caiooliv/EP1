#include "negativo.hpp"

Negativo::Negativo(){

}

list<Cor> Negativo::aplicaFiltro(list<Cor> cores, int escalaMaxima){

	//Declarações
	Cor pixel;
	unsigned char cont = escalaMaxima;

	//Instruções
	for(aux = cores.begin(); aux != cores.end();aux++){

		pixel = *aux;

		pixel.setR(cont - pixel.getR());
		
		pixel.setG(cont - pixel.getG());
		
		pixel.setB(cont - pixel.getB());
		
		cor.push_back(pixel);
	}

	return cor;
}