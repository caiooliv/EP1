#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <list>
#include "imagem.hpp"
#include "cor.hpp"
#include "filtro.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretobranco.hpp"

using namespace std;


int main(void){
		
	ifstream arquivo;
	Negativo *negativo;
	Polarizado *polarizado;
	PretoBranco *pretobranco;
	Imagem *imagem = new Imagem();
	int escolha;
	string nome;
	

	

	imagem->lerImagem(&arquivo);
	


	arquivo.close();



	cout<<"----------------------------------------------Cabeçalho---------------------------------------------------------------"<<endl;
	cout<<"\n\t\t\tFormato: " << imagem->getFormato() <<"\n\t\t\tDimensões: "<<imagem->getLargura()<<" X "<<imagem->getAltura()<<"\n\t\t\tEscala Maxima: "<< imagem-> getEscalaMaxima()<<"\n\t\t\tCabeçalho: "<< imagem-> getCabecalho()<<endl;
	cout<<"\n----------------------------------------------------------------------------------------------------------------------"<<endl;
	
	
	cout<<"----------------------------------------------Filtros-----------------------------------------------------------------"<<endl;
	cout<<"\n\n1-Negativo\n2-Polarizado\n3-Preto e Branco"<<endl;
	cout<<"\n----------------------------------------------------------------------------------------------------------------------"<<endl;
	cout<<"\nInforme qual filtro deseja aplicar:  ";
	cin>>escolha;
		
	while(escolha < 1 || escolha > 4 ){
		cout<<"----------------------------------------------Filtros-----------------------------------------------------------------"<<endl;
		cout<<"\n\nInforme qual filtro deseja aplicar\n1-Negativo\n2-Polarizado\n3-Preto e Branco"<<endl;
		cout<<"\n----------------------------------------------------------------------------------------------------------------------"<<endl;
		cin>>escolha;
	}	
 	
 		


	int opcao = escolha;

	
	switch(opcao){

		case(1):
			negativo = new Negativo();
			imagem->setCores(negativo->aplicaFiltro(imagem->getCores(), imagem->getEscalaMaxima()));
			
			break;
		case(2):
			polarizado = new Polarizado();
			imagem->setCores(polarizado->aplicaFiltro(imagem->getCores(), imagem->getEscalaMaxima()));
			
			break;
		
		case(3):
			pretobranco = new PretoBranco();
			imagem->setCores(pretobranco->aplicaFiltro(imagem->getCores(), imagem->getEscalaMaxima()));
			
			break;
	}
	

	imagem->novaImagem();

	arquivo.close();

	return 0;
	
}