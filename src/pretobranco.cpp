#include "pretobranco.hpp"

PretoBranco::PretoBranco(){

}

list<Cor> PretoBranco::aplicaFiltro(list<Cor> cores, int escalaMaxima){

//Declarações
	Cor pixel;
	unsigned char cont = escalaMaxima;
//Instruções
	for(aux = cores.begin(); aux != cores.end();aux++){

		pixel = *aux;

		cont =(0.299 * pixel.getR())+(0.587 * pixel.getG()) + (0.144 * pixel.getB());

		pixel.setR(cont);
		pixel.setG(cont);
		pixel.setB(cont);

		cor.push_back(pixel);
	}

	return cor;

}