#include "imagem.hpp"
#include "cor.hpp"

Imagem::Imagem(){

}

Imagem::Imagem(string formato, int altura, int largura, int escalaMaxima, list<Cor> cores){
	this->formato = formato;
	this->altura = altura;
	this->largura = largura;
	this->escalaMaxima = escalaMaxima;
	this->cores = cores;
}

string Imagem::getFormato(){
	return formato;
}

void Imagem::setFormato(string formato){
	this-> formato = formato;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setAltura(int altura){
	this-> altura = altura;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setLargura(int largura){
	this-> largura = largura;
}

int Imagem::getEscalaMaxima(){
	return escalaMaxima;
}

void Imagem::setEscalaMaxima(int escalaMaxima){
	this-> escalaMaxima = escalaMaxima;
}

list<Cor> Imagem::getCores(){
	return cores;
}

void Imagem::setCores(list<Cor> cores){
	this-> cores = cores;
}


string Imagem::getDiretorio(){
	return diretorio;
}

void Imagem::setDiretorio(string diretorio){
	this->diretorio = diretorio;
}



int Imagem::getCabecalho(){
	return cabecalho;
}

void Imagem::setCabecalho (int cabecalho){
	this-> cabecalho = cabecalho;
}

void Imagem::lerImagem(ifstream * arquivo){

	//Declarações
	string nome;
	string diretorio;
	Cor * cor = new Cor();
	unsigned char aux;
	//Instruções
	do{
		cout << "\tInforme o nome da imagem (sem a extensão) :";
		cin >> 	nome;
		
		
		diretorio = "doc/"+nome+".ppm";
		setDiretorio(diretorio);
		arquivo -> open(diretorio.c_str());


		if(arquivo->fail()){
			cout << "\n\t**Erro na abertura de arquivo, verifique se o nome do arquivo esta correto!**" << endl;
		}
	}while(arquivo->fail());

	ignorarComentario(arquivo);

	(*arquivo) >> this->formato;

	ignorarComentario(arquivo);

	(*arquivo) >> this->largura >> this->altura;	

	ignorarComentario(arquivo);

	(*arquivo) >> this->escalaMaxima;

	ignorarComentario(arquivo);	

	cabecalho = arquivo->tellg();
	cabecalho = cabecalho - 1;

	while (!arquivo->eof()){
		cor->setR(aux = arquivo->get());
		cor->setG(aux = arquivo->get());
		cor->setB(aux = arquivo->get());

		cores.push_back((*cor));

	}



}

void Imagem::ignorarComentario(ifstream * arquivo){
	
	//Declaração
	string linha;
	
	(*arquivo) >> linha;

	if(linha[0] != '#'){

			arquivo -> seekg (-linha.size(),ios_base::cur);
			return ;
	}

	while(linha[0] == '#'){

		getline(*arquivo,linha);

		(*arquivo ) >> linha;
	}

	arquivo -> seekg (-linha.size(),ios_base::cur);


}

void Imagem::novaImagem(){
	
	ifstream arquivo;
	ofstream novoArquivo;
	int tamanho = getCabecalho();
	string nome;
	


	arquivo.open(getDiretorio().c_str());

	
	do{
		cout << "Informe o nome da nova imagem com filtro (sem a extensão): ";
		cin >> nome;
		diretorio = "doc/"+ nome +".ppm";
		novoArquivo.open(diretorio.c_str());
		if(novoArquivo.fail()){
			cout<<"\n\t**Erro na abertura de arquivo, verifique se o nome do arquivo esta correto!**" << endl;
			cout<<"\n\t Informe novamente o nome da imagem"<<endl;
		}
	
	}while(novoArquivo.fail());
	

	while(arquivo.tellg() <= tamanho){
			
		novoArquivo.put((unsigned char) arquivo.get());
	}

	for(Cor cor : getCores()){
		
		novoArquivo.put(cor.getR());
		
		novoArquivo.put(cor.getG());
		
		novoArquivo.put(cor.getB());
	}


	
	cout << "\nO filtro foi aplicado com sucesso !" << endl;
}




	




		
